## Specify phone tech before including full_phone
$(call inherit-product, vendor/cm/config/gsm.mk)

# Release name
PRODUCT_RELEASE_NAME := speed

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/zte/speed/device_speed.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := speed
PRODUCT_NAME := cm_speed
PRODUCT_BRAND := zte
PRODUCT_MODEL := speed
PRODUCT_MANUFACTURER := zte
